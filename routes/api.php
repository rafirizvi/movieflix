<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\Api\AuthController;
use App\Http\Controllers\MovieController;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::post('/login', [AuthController::class, 'login']);
Route::get('/movie', [MovieController::class, 'index']);

// Protected Route:  only authanticated user can accss these URL's 
Route::group(['middleware' => ['auth:api','apiUserAuth']], function () {
    Route::get('/movie/{id}', [MovieController::class, 'show']);
    Route::get('/movielist', [MovieController::class, 'movielist']);
    Route::post('/movie', [MovieController::class, 'store']);
    Route::post('/movie/{id}', [MovieController::class, 'update']);
    Route::post('/logout', [AuthController::class, 'logout']);
});
