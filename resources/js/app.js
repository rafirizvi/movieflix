require('./bootstrap');

import Vue from 'vue'
import VueAxios from 'vue-axios';
import VueRouter from 'vue-router';
import axios from 'axios';

import router from './Router/index';
import store from './Store/index';
import App from './App.vue';


Vue.use(VueRouter);
Vue.use(VueAxios, axios);

const app = new Vue({
    el: '#app',
    router,
    store,
    components:{App},
    render: h => h(App),
});

// Reffrance
// https://onlinewebtutorblog.com/complete-laravel-8-vue-js-crud-tutorial/