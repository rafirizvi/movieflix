import Vue from 'vue'
import Vuex from 'vuex'
import MovieService from '../Services/MovieService';
Vue.use(Vuex)

const store = new Vuex.Store({

    state: {
        count: 0,
        movies: [],
    },

    getters: {
        movies: state => {
            return state.movies;
        }
    },

    mutations: {
        INCREMENT(state) {
            state.count++
        },
        SET_MOVIES_LIST(state, movies) {
            state.movies = movies
        }
    },

    actions: {
        async loadMovies({ commit }) {
            try {
                const response = await MovieService.getMovies();               
                commit('SET_MOVIES_LIST', response.data)
            }
            catch (error) {
                console.log(error);
            }
        }
    }

})

export default store;