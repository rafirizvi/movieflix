// import config from "../utils/config";

import config from "../utils/index";
const getMovies = async () => {
    try {        
        const response =  await axios.get(config.staticData.API_URL + '/movie');      
        return response.data;
      } catch (error) {
        console.log(error);
      }
 
}

export default {
    getMovies
}