<?php
use App\Providers\RouteServiceProvider;
use Laravel\Fortify\Features;
return [ 
    /****************************************************************************************************
     *                                              Error/Succcess Message
     *****************************************************************************************************/
    'common'=>[
        'UNAUTHORIZED'=>'You are not authorized to access this page',
        'UNAUTHORIZED_TYPE'=>'You are not authorized',
        'CREATE_USER '=>'User account has been created successfully.!',
        'DEFAULT_ERROR'=> 'Something went wrong!',
        'SERVER_ERROR'=> 'Internal server error.',
        'LOGIN_FAILED_EMAIL_NOT_VERIFIED'=> 'Please verify your email to login!',
        'AUTHORIZATION_REQUIRED'=> 'Access Denied - Authorization required',
        'ACTION_NOT_ALLOWED'=> 'User is not allowed to perform this action!',
        'NO_RECORDS'=> 'No record found!',
        'UPDATE_SUCCESS'=> 'Updated Successfully.',
        'CREATE_SUCCESS'=> 'Record Created Successfully.',
        'LOGIN_FAILED'=>'This User does not exist, check your details',
        'LOGOUT'=>'Successfully logged out',
        'CREATE_ERROR'=>'The movie details cant be updated. Please, try again.',
        'RECORD_FOUND'=>'Retrieved successfully',
        'LOGIN'=>'User login successfully.',
        'VALIDATION_ERROR'=>'Validation Error',
     ],
];