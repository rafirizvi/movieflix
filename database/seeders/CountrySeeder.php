<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\Country;
class CountrySeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Country::truncate();
        $countries = [
            ['name' => 'Australia', 'country_code' => 'AU'],
            ['name' => 'India', 'country_code' => 'IN'],
            ['name' => 'United States', 'country_code' => 'US'],
            ['name' => 'Algeria', 'country_code' => 'DZ'],
            ['name' => 'Taiwan', 'country_code' => 'TW'],
            ['name' => 'Tajikistan', 'country_code' => 'TJ'],
            ['name' => 'Tanzania', 'country_code' => 'TZ'],
            ['name' => 'Thailand', 'country_code' => 'TK'],
            ['name' => 'Tokelau', 'country_code' => 'AS'],
            ['name' => 'Tonga', 'country_code' => 'TO']
        ];

        foreach ($countries as $key => $value) {
            Country::create($value);
        }

    }
}
