<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\MovieGenre;
class MovieGenreSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        MovieGenre::factory(8)->create();
    }
}
