<?php

namespace Database\Factories;

use App\Models\MovieGenre;
use Illuminate\Database\Eloquent\Factories\Factory;
use App\Models\Movie;
use App\Models\Genre;
use Carbon\Carbon;
class MovieGenreFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = MovieGenre::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [          
            'movie_id' => rand(1, Movie::count()),
            'genre_id' => rand(1, Genre::count()),
            'created_at'=> Carbon::now(),
            'updated_at'=> Carbon::now()
        ];
    }
}
