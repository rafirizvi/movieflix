<?php

namespace Database\Factories;

use App\Models\Movie;
use Illuminate\Database\Eloquent\Factories\Factory;
use Carbon\Carbon;
use App\Models\User;
class MovieFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = Movie::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {        
        return [
            'user_id' => rand(1, User::count()),
            'name'   => $this->faker->sentence(5),
            'description'=>$this->faker->paragraph(1),  
            'release_date'=>Carbon::now(),
            'rating'=>rand(1,5),
            'ticket_price'=>250,
            'country_id'=>rand(1,5),
            'created_at'=> Carbon::now(),
            'updated_at'=> Carbon::now()
        ];
    }
}
