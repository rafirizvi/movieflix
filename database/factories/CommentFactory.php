<?php

namespace Database\Factories;


use Illuminate\Database\Eloquent\Factories\Factory;
use Carbon\Carbon;
use App\Models\Comment;
use App\Models\User;
use App\Models\Movie;

class CommentFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = Comment::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    { 
        return [
            'comment' => $this->faker->paragraph(1),
            'user_id' => rand(1, User::count()),
            'movie_id' => rand(1, Movie::count()),
            'created_at'=> Carbon::now(),
            'updated_at'=> Carbon::now()
        ];
    }
}
