<?php

namespace Tests\Feature;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;

class MovieTest extends TestCase
{

    public function testSuccessfulRegistration()
    {
    
        $response = $this->json('GET', 'api/movie',['Accept' => 'application/json'])
            ->assertStatus(200)
            ->assertJsonStructure([
                "data","message"
            ]);
    }

}
