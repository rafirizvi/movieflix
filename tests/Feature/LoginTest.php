<?php

namespace Tests\Feature;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Support\Facades\Auth;
use Tests\TestCase;
use App\Models\User;
class LoginTest extends TestCase
{
    /**
     * A basic feature test example.
     *
     * @return void
     */

    public function test_user_can_login_with_correct_credentials()
    {
        $userEmail = 'test_app'.rand(1,99).'@yopmail.com';
        $user = User::factory(1)->create([
            'email'=>$userEmail,
            'password' => bcrypt($password = 'Admin@123'),
        ]);

        $this->assertFalse(Auth::check());
        $data = [
            'email' => $userEmail,
            'password' => 'Admin@123',
        ];
        $response = $this->json('POST', 'api/login',$data,['Accept' => 'application/json'])
            ->assertStatus(200)
            ->assertJsonStructure([
                'data' => [
                    'access_token',
                    'user'
                ],"message"
            ]);
    }

}
