<?php

namespace Tests;
use Exception;
use Illuminate\Foundation\Testing\TestCase as BaseTestCase;
use Illuminate\Support\Facades\Artisan;

abstract class TestCase extends BaseTestCase
{
    use CreatesApplication;
    public function setUp(): void {
    
        parent::setUp();
        // Artisan::call('passport:install');
        // Artisan::call('migrate:fresh');
        // Artisan::call('db:seed');
    }
}
