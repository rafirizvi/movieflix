<?php

namespace App\Http\Controllers;

use App\Models\Movie;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;
use Illuminate\Validation\ValidationException;
use App\Http\Resources\ProjectResource;
use App\Services\MovieService;
use App\Http\Controllers\Api\BaseController as BaseController;

class MovieController extends BaseController
{
    
    /**
     * Global Variable for movie controller
     */

    protected $movies;
    protected $common; // variable for common error 

    /**
     * Create a new controller instance.
     *
     * @param  MovieService  $users
     * @return void
     */
    public function __construct(MovieService $movies)
    {
        $this->movies = $movies;       
        $this->errorList = config('customError');      
    }


    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {                 
        $movies = $this->movies->getMovieListing();
        $message = ($movies) ? $this->errorList['common']['RECORD_FOUND']  : $this->errorList['common']['NO_RECORDS'] ;
        return $this->sendResponse(ProjectResource::collection($movies), $message);     
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function movielist()
    {   
        $movies = $this->movies->getMovieListByUserId();
        $message = ($movies) ? $this->errorList['common']['RECORD_FOUND']  : $this->errorList['common']['NO_RECORDS'] ;
        return $this->sendResponse(ProjectResource::collection($movies), $message);     
    }
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
       
        try{
            $data = $request->all();
            $rules = array(
                'name' => ['required', 'string'],
                'user_id' => ['required','numeric'],
                'description' => ['required','string'],
                'release_date' => ['required','date_format:Y-m-d'],
                'rating' => ['required','numeric','min:1','max:5'],
                'ticket_price' => ['required','numeric'],
                'country_id' => ['required','numeric'],
                'genre' => ['required', 'string'],
                'photo' => ['required','image','mimes:jpeg,jpg,png'],
            );
            $validate = Validator::make($data, $rules);
            if ($validate->fails()) {
                return $this->sendError($this->errorList['common']['VALIDATION_ERROR'], $validate->errors());
            }else {
                $result = $this->movies->createMovie($data,$message);
                if($result){ 
                    return $this->sendResponse([], $message);
                } 
            }
        }catch(\Exception $e){
            return $this->sendError($this->errorList['common']['DEFAULT_ERROR'], $e->getMessage());
        }
    } 

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */

    public function show($id)
    {        
        $movie = $this->movies->getMoveDetailByID($id);
        $message = ($movie) ? $this->errorList['common']['RECORD_FOUND']  : $this->errorList['common']['NO_RECORDS'] ;
        return $this->sendResponse(ProjectResource::collection($movie), $message);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Movie  $movie
     * @return \Illuminate\Http\Response
     */
    public function edit(Movie $movie)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  $movieid
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $movieid)
    {   
        try{
            $data = $request->all();
            $rules = array(
                'name' => ['required', 'string'],
                'user_id' => ['required','numeric'],
                'description' => ['required','string'],
                'release_date' => ['required','date_format:Y-m-d'],
                'rating' => ['required','numeric','min:1','max:5'],
                'ticket_price' => ['required','numeric'],
                'country_id' => ['required','numeric'],
                'genre' => ['required', 'string'],
                'photo' => ['required','image','mimes:jpeg,jpg,png'],
            );
            $validate = Validator::make($data, $rules);
            if ($validate->fails()) {
                return $this->sendError($this->errorList['common']['VALIDATION_ERROR'], $validate->errors());
            }else {
                $result = $this->movies->updateMovie($data,$movieid,$message);
                return $this->sendResponse([], $message);
            }
        }catch(\Exception $e){
            return $this->sendError($this->errorList['common']['DEFAULT_ERROR'], $e->getMessage());
        }   
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Movie  $movie
     * @return \Illuminate\Http\Response
     */
    public function destroy(Movie $movie)
    {
        //
    }
}
