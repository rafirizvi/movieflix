<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Auth;
use Illuminate\Auth\RequestGuard;
use App\Http\Resources\ProjectResource;
use Carbon\Carbon;
use App\Models\User;
use App\Http\Controllers\Api\BaseController as BaseController;

class AuthController extends BaseController
{
    

    /**
     * Verify the user credential for application access.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function login(Request $request) {
        $loginData = $request->validate([
            'email' => 'email|required',
            'password' => 'required'
        ]);

        if (!auth()->attempt($loginData)) {
            return $this->sendError('Unauthorised.', ['error'=>config('customError.common.LOGIN_FAILED')]);
        }
        $result['access_token'] =  auth()->user()->createToken('authToken')->accessToken;
        $result['user'] =  auth()->user()->id;
       return $this->sendResponse($result, config('customError.common.LOGIN'));

    }

    /**
     * destroy the user session.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function logout(Request $request) {
        if(Auth::check()){
            $request->user()->token()->revoke(); 
            return $this->sendResponse([], config('customError.common.LOGOUT'));
        }else{ 
            return $this->sendError('Unauthorised.', ['error'=>config('customError.common.UNAUTHORIZED_TYPE')]);
        } 
    }    
}
