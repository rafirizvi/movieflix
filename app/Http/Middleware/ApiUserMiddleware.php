<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;
use Illuminate\Validation\ValidationException;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Redirect;
class ApiUserMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle(Request $request, Closure $next)
    {     
       
       try{
            if(Auth::user()){
                return $next($request);
            }else{
                return response()->json([ 'error' => 'Unauthorised', 'message' => config('customError.common.UNAUTHORIZED_TYPE')],401);
            }          
        }catch (\Exception $e){
            return response()->json([ 'error' => 'Unauthorised', 'message' => config('customError.common.UNAUTHORIZED_TYPE')],401);
        }
    }
}
