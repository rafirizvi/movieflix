<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use App\Models\User;
use App\Models\Comment;
use App\Models\Country;
use App\Models\MovieGenre;
use App\Models\Genre;
class Movie extends Model
{
    use HasFactory;
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'user_id',
        'name',
        'description',
        'release_date',
        'rating',
        'ticket_price',
        'country'
    ];

    /************************************************************************************************************
     *                                          Eloquent: Relationships
     ************************************************************************************************************/

    /**
     * Get the user that owns the movies.
     */
    public function user()
    {
        return $this->belongsTo(User::class, 'user_id', 'id');
    }

    /**
     * Relationship between movie and Comments model    
     * @return object
     */
    
    public function comment()
    {
        return $this->hasMany(Comment::class, 'movie_id', 'id');
    }  
    
    /**
     * Relationship between movie and Country model    
     * @return object
     */
    
    public function country()
    {
        return $this->hasOne(Country::class, 'id', 'id');
    }  

    /**
     * Relationship between movie and Movie Genre model    
     * @return object
     */
    
    public function movie_genre()
    {
        return $this->hasMany(MovieGenre::class, 'movie_id', 'id');
    }
}
