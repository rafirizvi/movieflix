<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use App\Models\Movie;

class Country extends Model
{
    use HasFactory;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [       
        'name',
        'country_code'
    ];

     /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'country_code',
        'flag',
        'created_at',
        'updated_at',
    ];

    /**
     * Relationship between movie and Comments model    
     * @return object
     */
    
    public function movie()
    {
        return $this->belongsTo(Movie::class, 'country_id', 'id');
    }

}
