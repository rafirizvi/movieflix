<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use App\Models\Movie;
use App\Models\MovieGenre;
class Genre extends Model
{
    use HasFactory;

     /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [       
        'name'
    ];

     /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [       
        'flag',
        'created_at',
        'updated_at',
    ];

    /************************************************************************************************************
     *                                          Eloquent: Relationships
     ************************************************************************************************************/

    /**
     * Relationship between movie and Country model    
     * @return object
     */
    
    public function movie_genre()
    {
        return $this->hasOne(MovieGenre::class, 'genre_id', 'id');
    }
    
}
