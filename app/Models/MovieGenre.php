<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use App\Models\Movie;
use App\Models\Genre;
class MovieGenre extends Model
{
    use HasFactory;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [      
        'movie_id',
        'genre_id'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [       
        'is_deleted',
        'created_at',
        'updated_at',
    ];


     /************************************************************************************************************
     *                                          Eloquent: Relationships
     ************************************************************************************************************/

     /**
     * Relationship between movie genre and genre model    
     * @return object
     */
    
    public function genre()
    {
        return $this->belongsTo(Genre::class, 'genre_id', 'id');
    }
}
