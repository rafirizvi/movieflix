<?php

namespace App\Services;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use Illuminate\Validation\ValidationException;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Str;
use Carbon\Carbon;
use Storage;
use DB;
use File;

// Modal
use App\Models\Movie;
use App\Models\Country;
use App\Models\Comment;
use App\Models\Genre;
use App\Models\MovieGenre;
use App\Models\User;


class MovieService {


    /**
     * Global Variable for movie service
     */
    protected $currentUserDetails;
    protected $movies;
    protected $countries;
    protected $comments;
    protected $genres;
    protected $movieGenre;
    protected $users;

    /**
     * Create a new Movie Service instance.
     *
     * @return void
     */

    public function __construct() {
        /**
         * Create the related modal Object
         */
        $this->currentUserDetails = Auth::user();
        $this->countries = new Country();
        $this->movies = new Movie();
        $this->comments = new Comment();
        $this->genres = new Genre();
        $this->movies = new Movie();
        $this->movieGenre = new MovieGenre;
        $this->users = new User();
        $this->errorList = config('customError');
    }

    /**
     * [getMovieListing] we are getiing all the movies listing
     * @param  NA
     * @param  NA
     * @return dataArray
     */
    public function getMovieListing(){
        $moviesArray =  $this->movies->with(['user:id,name','comment','country','movie_genre'=>function($q){
                            $q->with('genre');
                        }])                  
                        ->orderBy('id','DESC')->paginate(10);
        return $moviesArray;
    }

    /**
     * [getMovieListByUserId] we are getiing all the movies listing belongs to current user
     * @param  NA
     * @param  NA
     * @return dataArray
     */
    public function getMovieListByUserId(){
        $moviesArray =  $this->movies->with(['user:id,name','comment','country','movie_genre'=>function($q){
                            $q->with('genre');
                        }])  
                        ->where('user_id',Auth::user()->id)                
                        ->orderBy('id','DESC')->paginate(10);      
        return $moviesArray;
    }

    /**
     * [createMovie] we are storing the movie Details into DD
     * @param  requestInput get all the requested input data
     * @param  message return message based on the confition 
     * @return dataArray with message
     */

    public function createMovie($input,&$message=''){
        try{
            $getImageArray = $this->uploadImage($input,false);
            $this->movies->name = trim($input['name']);
            $this->movies->user_id = $input['user_id'];
            $this->movies->description = $input['description'];
            $this->movies->release_date = $input['release_date'];
            $this->movies->rating = (int)$input['rating'];
            $this->movies->ticket_price = $input['ticket_price'];
            $this->movies->country_id = $input['country_id'];
            $this->movies->photo = ($getImageArray['status']) ? $getImageArray['photo'] :'';
            $this->movies->created_at = Carbon::now();
            $this->movies->updated_at = Carbon::now();           
            if($this->movies->save()){
                $this->saveGenreForMovie($this->movies->id,$input['genre'],false);
                $message = $this->errorList['common']['CREATE_SUCCESS'];
                return true;
            }          
        }catch(\Exception $e){
            
            $message=$this->errorList['common']['CREATE_ERROR'];
            return false;
        }
    }

    /**
     * [updateMovie] we are updating the movie Details into BD
     * @param  requestInput get all the requested input data
     * @param  movieId Movie is which data need to be updatec
     * @param  message return message based on the confition 
     * @return dataArray with message
     */
    public function updateMovie($input,$movieId,&$message=''){               
        try{
            $movies = $this->movies->find($movieId);            
            if($movies){
                if($movies->user_id == Auth::user()->id){
                    $getImageArray = $this->uploadImage($input,$movies->photo);
                    $movies->name = trim($input['name']);
                    $movies->user_id = $input['user_id'];
                    $movies->description = $input['description'];
                    $movies->release_date = $input['release_date'];
                    $movies->rating = (int)$input['rating'];
                    $movies->ticket_price = $input['ticket_price'];
                    $movies->country_id = $input['country_id'];     
                    $movies->photo = ($getImageArray['status']) ? $getImageArray['photo'] :'';
                    if($movies->save()){
                        $this->saveGenreForMovie($movieId,$input['genre'],true);
                        $message = $this->errorList['common']['CREATE_SUCCESS'];
                        return true;
                    }
                }else{
                    
                    $message=$this->errorList['common']['AUTHORIZATION_REQUIRED'];
                    return false; 
                }
                
            }else{
                $message=$this->errorList['common']['CREATE_ERROR'];
                return false;
            }        
        }catch(\Exception $e){
            
            $message=$this->errorList['common']['CREATE_ERROR'];
            return false;
        }
    }

    

    /**
     * [saveGenreForMovie]
     * @param  int $id [movie id which data need to be return]
     * @param  string &$genre    [we are getting comma saprated genre value ]
     * @return $object
     */
    public function saveGenreForMovie($id,$genre,$isUpdate){
        // if is update true then delete previous genre which related to movie id
        if($isUpdate){
            $this->movieGenre->where('movie_id',$id)->delete(); 
        }
        $finalArray = array();        
        $genreArray = explode(',', $genre); 
        foreach($genreArray as $key=>$value){
            array_push($finalArray,array(
                'movie_id'=>$id,
                'genre_id'=>(int)$value,
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now()
            ));
        }      
        if($this->movieGenre->insert($finalArray)){
           return $this->movieGenre;
        } 
    }

    /**
     * [getMoveDetailByID]
     * @param  int $id [movie id which data need to be return]
     * @param  string &$message    [description ]
     * @return $object
     */
    public function getMoveDetailByID($id){      
        $movie =  $this->movies->with(['user:id,name','comment','country','movie_genre'=>function($q){
                            $q->with('genre');
                        }])
                        ->where('id',$id)
                        ->orderBy('id','DESC')
                        ->get();
        return $movie;
    }

    /**
     * upload image into directory
     * @param  object  $input
     * @return object array
     */
    public function uploadImage($input,$fileName){
       
        // if photo already exist then delete the previous one       
        if($fileName){ 
            File::delete(public_path("/storage/images/".$fileName));
        }
        $returnArray = [];
        $path = public_path()."/storage/images/";
        $timeDate = strtotime(Carbon::now()->toDateTimeString()); 
        $returnArray['status'] = false;
        if(isset($input['photo']) && !empty($input['photo'])){
            $requestImageName = $input['photo'];
            $imageNameWithExt = $requestImageName->getClientOriginalName();
            $filename = pathinfo($imageNameWithExt, PATHINFO_FILENAME); 
            $ext = $requestImageName->getClientOriginalExtension();
            $image_name = $timeDate.'_'.rand().'.'.$ext;
            $image_path = 'images/'.$image_name;
            if(!$requestImageName->move($path,$image_name)){
                $returnArray['status'] = false;
            }else{
                $returnArray['status'] = true;
                $returnArray['photo'] = $image_name;
            }
        }
        return $returnArray;
    }

    

}