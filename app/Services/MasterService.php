<?php
namespace App\Services;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Validation\ValidationException;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Str;

use App\Models\Country;
use App\Models\Genre;

class MasterService {

    /**
     * Global Variable for movie service
     */
    protected $currentUserDetails;
    protected $countries;
    protected $genres;

    /**
     * Create a new Movie Service instance.
     *
     * @return void
     */

    public function __construct() {
        /**
         * Create the related modal Object
         */
        $this->currentUserDetails = Auth::user();
        $this->countries = new Country();
        $this->genres = new Genre();
        $this->errorList = config('customError');
    }

    /**
     * [getCountries] we are getiing all the countries
     * @param  
     * @param  
     * @return dataArray
     */
    public function getCountries(){
        return $this->countries->select('id', 'name')->orderBy('name','asc')->get();
    }

    /**
     * [getGenres] we are getiing all the Genres
     * @param  
     * @param  
     * @return dataArray
     */
    public function getGenres(){
        return $this->countries->select('id', 'name')->orderBy('name','asc')->get();
    }
    
}