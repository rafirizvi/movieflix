# Laravel 8 API project setup with migration,seed ,Vue.Js

Laravel 8 API project setup with migration,seed ,Vue.Js

## Requirement
```bash
PHP ^7.3
Composer 2 
Laravel ^8
```

## Repo Setup

Clone the repo on your local PC: -

```bash
git clone https://rafirizvi@bitbucket.org/rafirizvi/movieflix.git

cd movieflix

composer install
```
##  Setup .ENV and DB

```bash
cp .env.example .env 

#After DB setup run below command to genrate the laravel key 

php artisan key:generate

php artisan config:cache

```
## Seed and Migration

```bash
# After running the above command now we have to migrate the table and seed the data into the table.

 php artisan migrate:fresh
 php artisan db:seed
 php artisan passport:install
 
```
## Clean yourcache memory before run the application.

```bash
 php artisan route:clear 
 php artisan cache:clear
 php artisan config:clear
```
## Test Case
```bash
# Now time to test our application if all the command and route working fine.
php artisan test
```
## Serve Laravel Application 

```bash
 php artisan serve
```
## Vue.js 
```bash
# Run NPM Installer
npm install

#Run vue.js application
npm run dev #for development

#for dev with live changes
npm run watch 

#for production
npm run prod 

#for more details look at package.json file

```


Please make sure to update tests as appropriate.

## License
[MIT](https://choosealicense.com/licenses/mit/)